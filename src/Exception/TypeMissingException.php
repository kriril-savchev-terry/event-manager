<?php

namespace Terry\Event\Exception;

use LogicException;

class TypeMissingException extends LogicException implements
    EventExceptionInterface
{

}
