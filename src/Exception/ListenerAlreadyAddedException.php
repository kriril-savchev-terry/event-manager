<?php

namespace Terry\Event\Exception;

use LogicException;

class ListenerAlreadyAddedException extends LogicException implements
    EventExceptionInterface
{

}
