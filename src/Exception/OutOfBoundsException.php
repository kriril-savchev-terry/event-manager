<?php

namespace Terry\Event\Exception;

class OutOfBoundsException extends \OutOfBoundsException implements
    EventExceptionInterface
{

}
