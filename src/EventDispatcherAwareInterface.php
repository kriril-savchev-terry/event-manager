<?php

namespace Terry\Event;

use Psr\EventDispatcher\EventDispatcherInterface;

interface EventDispatcherAwareInterface
{

    public function setEventDispatcher(EventDispatcherInterface $dispatcher);

    public function getEventDispatcher(): ?EventDispatcherInterface;
}
