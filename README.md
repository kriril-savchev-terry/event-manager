# Terry Event Manager

Simple PSR-14 event manager. While PSR-14 is still in draft status, this package
provides the standard interfaces on its own. Once the standard is accepted,
the official interfaces will be included to the package.

## Implementation

The package implements the [PSR-14](https://github.com/php-fig/fig-standards/blob/master/proposed/event-dispatcher.md ).
It includes two interfaces and their implementation, respectively for Event and
EventManager objects. Also includes a class trait for event manager holding
classes, utilizing the getter and setter of the manager and bypassing the event
triggering.

## Examples

Simple attach and trigger an event listener:

```php
<?php

use Terry\EventManager\EventManager;
use Terry\EventManager\EventInterface;

// create the event manager:
$eventManager = new EventManager();

// create the event listener:
$callbackListener = function(EventInterface $e) {
    echo $e->getName().PHP_EOL;
};

// attach the listener to the manager:
$eventManager->attach('testEvent', $callbackListener);

// some other code...
// trigger the listeners attached to 'testEvent':

$eventManager->trigger('testEvent'); // Should output 'testEvent'
```

Attach multiple listeners and stop the event propagating:

```php
<?php

use Terry\EventManager\EventManager;
use Terry\EventManager\EventInterface;

// first listener:
$listener1 = function(EventInterface $e) {
    echo 'In first listener'.PHP_EOL;
};
// second listener that will stop the event propagating:
$listener2 = function(EventInterface $e) {
    echo 'In second listener'.PHP_EOL;
    $e->stopPropagation(true);
    echo 'Event '.$e->getName().' is stopped!';
};
// third listener that will not be executed:
$listener3 = function($e) {
    echo 'This listener won\'t be triggered'.PHP_EOL;
};
// create the event manager:
$eventManager = new EventManager();

// attach the listeners to 'testEvent':
$eventManager->attach('testEvent', $listener1);
$eventManager->attach('testEvent', $listener2);
$eventManager->attach('testEvent', $listener3);

// trigger the 'testEvent':
$eventManager->trigger('testEvent');

// the output should be:
// 'In first listener'
// 'In second listener'
// 'Event testEvent is stopped!'

```
